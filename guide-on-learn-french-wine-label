If you have ever savoured a glass of Champagne, Burgundy or Bordeaux, you might have tried to decode the strange words written on their labels. French wines come with labels that contain pertinent information related to the wine, because of the strict labelling laws in France. They are mostly labelled by region and not always by the grape varieties. Interpreting the terms in the label can be helpful if you are trying to add some French wines to your collection.

To decipher a French wine label, you need not depend on a French dictionary or learn French. Explained below are the common details that you can find on a French wine. Keep these handy before you buy wine online:

Vintage
This is the simplest of all information that can be found on a French red wine or white wine label. It shows the year in which the wine grapes were harvested.

Producer
This is usually the second most important detail in a wine label. It usually showcases the name of the winery along with any logo that represent them.

Appellation and/or Varietal
Appellation displays the region in France where the wine grapes were grown. Some winemakers list the varietal or grape varieties used. If the bottle shows only appellation, it indicates that the region is known only for growing one variety of wine grapes.

Some appellations also showcase classification of the wine. For example, Grand Cru and Premier Cru are two common classifications, and show the superiority of the wine quality. While classification is not available for all appellations, a quick research about the popular wine regions in France can give you an insight into whether the one you are looking at is worth buying.

Wine makers growing grapes in classified appellations usually include that information on their wine bottles. If you are unsure of the appellation, search for the acknowledged name for the type of wine you are buying. For example, if you are buying a Burgundy wine, search for more information on regions renowned for making Burgundy wines. If the bottle you are looking at shows any of the well-known appellations, acquiring the bottle is a safe bet.

Winery Address
This is the address of the vineyard owner. It isn’t the same as the estate where it was bottled. Wine producers display estate information only if the wine was bottled on the estate the winery belongs to, since this can be a unique selling point for their wines. If the wine producer retires or if the estate has a new owner, the winery address can change. You can use the specific winemaker’s information to look up how collectible the wine is. 

Pertinent information like the winemaker’s name and the name and address of the estate where the wine was bottled, could add great value to the wine. This could also indicate how the wine tastes and how much its value is in the market.

Alcohol Content and Volume
On most French white wine or red wine bottles, the amount of alcohol content and volume of the bottle will be printed at the bottom of the label, either on the back or on the front of the bottle. The percentage value will be shown for alcohol content, and the volume will be entered in mL.

What to Really Look For?
While the aforementioned details can give you an insight into the type of wine you are going to buy, only a few categories can help you figure if the wine is worth the collection. Focus on appellation, classification of the appellation, estate where the wine was bottled, and winery address or owner information if you want to make a decision on buying an unfamiliar wine.

For champagnes, ranking is based on the villages where they are made. All producers belonging to the same village are ranked the same. The best collectible champagnes will have a ranking of above 90. Grand Cru and Premier Cru makes the highest quality wines. If the champagne bottle mentions either of these two names, it is worth buying.